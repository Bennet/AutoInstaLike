/*
 * AutoInstaLike
 * 
 * You can edit the following variables:
 * - likeMode = there are 2 modes; 1. like on overview and profile page, 2. like only on profile page
 * - maxLikes = maximum of posts that will be liked
 * - minLikes = minimum of posts that will be liked
 * - randMaxLikes = if true, random number between minLikes and maxLikes -> that number of posts will be liked otherwise const maxLikes.
 * - profileLikes = if true, opens profile of liked overview post and like other posts from same profile
 * - maxProfileLikes = the number of posts that will be liked of the same profile
 * - maxFFRatio = ratio of follower and following of the user profile to check, if a follow could be usefull (f/f=ratio)
 * - autoSkip = auto skip if bad ratio
 */
// START EDITING
const likeMode          = 2;
const maxLikes          = 90;
const minLikes          = 70;
const randMaxLikes      = true;
const profileLikes      = true;
const maxProfileLikes   = 5;
const maxFFRatio        = 1.25;
const autoSkip          = true;

// variables for selectors (must be edit, if insta change something on their site)
const selectNodeList    = '.wpO6b';
const selectProfile     = '.Ppjfr .e1e1d a';
const selectProfileStats= '.wW3k- .-nal3';
const selectSubscribe   = '.Ppjfr .bY2yH button';
const selectPosts       = '.Nnq7C:first-child .v1Nh3 a';
// END EDITING

// set variables for working script
window.autoInstaLike = {
    likes: 0,
    maxLikes: (randMaxLikes) ? Math.floor(Math.random() * (maxLikes - minLikes) ) + minLikes : maxLikes,
    blocked: false,
    likeMoreFromProfile: (profileLikes) ? true : false, 
    runtime: 0,
    profileWindow: null,
    profileActions: null,
    profileWindowLikes: 0,
    profileWindowMaxLikes: (maxProfileLikes) ? maxProfileLikes : 3,
    profileWindowFinished: null,
    profileWindowSetFollow: false,
    profileWindowMethods: {
        openPost: function(thisProfileWindow) {
            const pictures  = thisProfileWindow.document.querySelectorAll(selectPosts);
            const pic       = ( 1 === likeMode ) ? 1 : 0;
            pictures[pic].click();
        },
        likePosts: function( thisProfileWindow ) {

            // loop the like function
            var like = thisProfileWindow.setInterval( likeLoop, 5000 );

            function likeLoop() {
                // check if max likes are reached
                if ( ail.likes < ail.maxLikes && ail.profileWindowLikes < ail.profileWindowMaxLikes ) {
                    
                    // get objects from profile window
                    const node_list = thisProfileWindow.document.querySelectorAll(selectNodeList);
                    // strange, but need to take different objects, if the first or second picture on a profile is open
                    const heart = ( 2 === likeMode && 0 === ail.profileWindowLikes ) ? node_list[4] : node_list[5];
                    const arrow = ( 2 === likeMode && 0 === ail.profileWindowLikes ) ? node_list[2] : node_list[3];

                    if ( checkHeart(heart) ) {
                        // BE MORE HUMAN - wait 1s an like the post
                        setTimeout(() => {
                            heart.click();
                            ail.likes++;
                            ail.profileWindowLikes++;
                            logLikes();
                            
                        }, 1000);
                    }

                    if ( ail.profileWindowLikes + 1 < ail.profileWindowMaxLikes ) {
                        // BE MORE HUMAN - wait 2s and move to the next post
                        setTimeout(() => {
                            arrow.click();
                        }, 2000);
                    }

                } else {
                    if ( true === ail.profileWindowSetFollow ) {
                        subscribe(ail.profileWindow);
                    }
                    // BE MORE HUMAN - reset & close profile window
                    setTimeout(() => {
                        clearLikeLoop(like);
                    }, 1000);
                    
                }
            }
        },
        botomBar: function(thisProfileWindow) {
            const profileStats  = thisProfileWindow.document.querySelectorAll(selectProfileStats);

            // follower
            var follower = profileStats[1].children[0].innerHTML;

            // following
            var following = profileStats[2].children[0].innerHTML;

            // convert data in correct format
            let replace = {};
            replace['.'] = '';
            replace[','] = '';
            replace['k'] = '00';
            replace['m'] = '00000';

            for(let key in replace){
                follower = follower.replace(key, replace[key]);
                following = following.replace(key, replace[key]);
            }

            // calculate follower/following ratio
            var ratio    = follower / following;
            var badRatio = ( ratio < maxFFRatio ) ? false : true;       

            // create new div in profile window
            var div = thisProfileWindow.document.createElement("div");
            div.classList.add("ail-bottom-bar");
            div.style.position = 'fixed';
            div.style.bottom = 0;
            div.style.display = 'flex';
            div.style.width = '100%';
            div.style.height = '50px';
            div.style.justifyContent = 'center';
            div.style.textAlign = 'center';
            div.style.fontSize = '20px';
            div.style.color = '#fff';
            div.style.backgroundColor = ( ! badRatio ) ? 'green' : '#c30000';
            div.style.zIndex = 999;

            div.innerHTML = 'Follower: ' + follower + ' Following: ' + following + ' Ratio: ' + Math.round(ratio * 100) / 100;;
            thisProfileWindow.document.querySelector("body").appendChild(div);
            // auto skip if activated and bad ratio
            if ( autoSkip && badRatio ) {
                clearLikeLoop();
            } 
        },
    },
}

ail = window.autoInstaLike;

// start time counter
runtime();

console.log(`Max Likes: ${ail.maxLikes}`);

/**
 *  Run inital checking script every second
 * 
 */ 
const run = setInterval(() => {

    // kill the interval if maxLikes are reached
    if ( ail.likes === ail.maxLikes ) {
        clearInterval(run);
        return;
    }

    // check if state is blocked = don't excecute Magic script again.
    // wait until magic is done and blocked = false;
    if ( ! ail.blocked ) { 
        // do magic will be executed, set state to blocked, that it don't fires again until magic happened
        ail.blocked = true;
        ail.profileWindowFinished = false;
        // start actions on the overview page
        overviewActions();

    } else {
        // if script in profile window finished, set general block state to false
        if ( ail.profileWindowFinished === true ) {
            
            const node_list = document.querySelectorAll(selectNodeList);
            const arrow     = node_list[2];

            arrow.click();
            ail.blocked = false;

        }
    }

    // close and skip profile, if RETURN is pressed, subscribe if ENTER is pressed
    if ( ail.profileWindow ) {
        ail.profileWindow.onkeyup = function(e) {
            if ( 8 === e.keyCode ) {
                clearLikeLoop();
            } else if ( 13 === e.keyCode ) {
                var bottomBar = ail.profileWindow.document.querySelectorAll(".ail-bottom-bar");

                if ( ! ail.profileWindowSetFollow ) {
                    ail.profileWindowSetFollow = true;
                    bottomBar[0].style.backgroundColor = 'blue';
                } else {
                    ail.profileWindowSetFollow = false;
                    bottomBar[0].style.backgroundColor = 'green';
                }
                
            }
        };
    }

}, 100);

/**
 * Main function loop the overview page
 * Checks the mode, post, like or move to next post. 
 * If activated open the profile and like other posts from same user.
 */
function overviewActions() {
    
    // check if maxLikes reached
    if ( ail.likes < ail.maxLikes ) {
       
        // BE MORE HUMAN - wait 1s before like the post in the overview
        setTimeout(() => {

            // get objects from page
            const node_list = document.querySelectorAll(selectNodeList);
            const profile   = document.querySelectorAll(selectProfile);
            const arrow     = node_list[2];
            const heart     = node_list[4];

            if ( checkHeart(heart) ) {

                // like on overview
                if ( 1 === likeMode) {
                    heart.click();
                    ail.likes++;
                    logLikes();
                }

                if ( ail.likeMoreFromProfile && ail.likes < ail.maxLikes ) {
                    likeOnProfilePage(profile);
                } 
                
            } else {
                arrow.click();
                ail.blocked = false;
            }
            
        }, 1000);
    }
}

/**
 * If likeMoreFromProfile is activated, the script will open the profil page 
 * of the user and like other posts from it.
 * 
 * @param {Object} profile 
 */
function likeOnProfilePage(profile) {
   
    // BE MORE HUMAN - wait 1s, open the profile in new tab
    setTimeout(() => {
        ail.profileActions = true;
        ail.profileWindow = window.open( profile[0].href, "_blank" );
    
        // wait until new window is loaded
        ail.profileWindow.addEventListener("load", function(event) {
            
            // insert botom bar
            ail.profileWindowMethods.botomBar(this);
            
            // open post on profile page
            setTimeout(() => {
                ail.profileWindowMethods.openPost(this);
            }, 250);
            
            // BE MORE HUMAN - wait 1s and start linking script
            setTimeout(() => {
                ail.profileWindowMethods.likePosts(this);
            }, 1000);

        });

    }, 1000 );
}

/**
 * Check if heart button exist and cointains 2 antoher div's.
 * If not, than the post is already liked and skip to the next one.
 * 
 * @param {Object} heart 
 * @returns {Boolean} likedPossible
 */
function checkHeart(heart) {

    likedPossible = ( heart && heart.children.length === 2 ) ? true : false;

    return likedPossible; 
}

/**
 * Write the current likes and max likes into the console
 */
 function logLikes() {
    console.log(`Likes: ${ail.likes} / ${ail.maxLikes}`);
}

/**
 * Count the runtime of the script until max likes are reached
 */
function runtime() {
    const time = setInterval(() => {
        ail.runtime++;

        if ( ail.likes === ail.maxLikes ) {
            console.log(`Finished in: ${convertTime(ail.runtime)}`);
            clearInterval(time);
        }

    }, 1000 );
}

/**
 * Convert seconds into minutes + seconds
 * 
 * @param {Integer} sec 
 * @returns {String}
 */
function convertTime(sec) {
    var min = Math.floor(sec/60);
    (min >= 1) ? sec = sec - (min*60) : min = '00';
    (sec < 1) ? sec='00' : void 0;

    (min.toString().length == 1) ? min = '0'+min : void 0;    
    (sec.toString().length == 1) ? sec = '0'+sec : void 0;    
    
    return min+':'+sec;
}

/**
 * Skip the current profile / clear the like loop
 */
function clearLikeLoop(interval = ail.profileWindow) {
    ail.profileWindow.close();
    ail.profileWindowLikes = 0;
    ail.profileWindowSetFollow = false;
    ail.profileWindowFinished = true;
    ail.profileWindow = null;
    clearInterval(interval);
}

/**
 * Subscribe current profile
 */
function subscribe(window) {
    const subscribeBtn = window.document.querySelectorAll(selectSubscribe);
    subscribeBtn[0].click();
}