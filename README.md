# AutoInstaLike

This is a short script to automatically like Instagram posts and follow accounts.
It's only working in the web version of instagram so you need to use your computer.

## Options
The available options are documented at the top of the file `autoinstalike.js`.

## How to use
To use this you have to:
1. Login with your account in the web version of Instagram
2. Copy the content of `autoinstalike.js`
3. Open the [web developer console](https://www.computerhope.com/issues/ch002153.htm) of your browser.
4. Paste the content into the `console`
5. Optionally edit settings (documented at the top of the file contents)
6. Hit enter, let the script run and enjoy the automation

To stop that script from executing just reload the page.

## Troubleshooting
If something doesn't work (anymore), Instagram probably changed something on their website so that the selectors of the script aren't correct anymore.
They have to be updated in the script (at the top of the file contents) to work again. Therefore some HTML knowledge is necessary to get the right one.
f you have encountered this problem, please let me know so I can update the selectors in this repository.

:warning: Instagram is not a friend of such scripts :warning:
Instagram may notice the use of such automation and block the account. 
**Use this script is at your own risk.**